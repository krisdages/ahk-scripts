!LButton::
SetWinDelay,0
SetBatchLines,-1
CoordMode,Mouse,Screen
MouseGetPos,oldmx,oldmy,mwin,mctrl
Loop
{
  GetKeyState,lbutton,LButton,P
  GetKeyState,alt,Alt,P
  If (lbutton="U" Or alt="U")
    Break
  MouseGetPos,mx,my
  WinGetPos,wx,wy,ww,wh,ahk_id %mwin%
  wx:=wx+mx-oldmx
  wy:=wy+my-oldmy
  WinMove,ahk_id %mwin%,,%wx%,%wy%
  oldmx:=mx
  oldmy:=my
}
Return