~LButton & RButton::
Run, "dexpot\sudexpot-d.lnk", , Hide ;%ComSpec% "dexpot\sudexpot -V", ProgramFiles, Hide
return
~RButton & LButton::
Run, "dexpot\sudexpot-V.lnk", , Hide ;%ComSpec% "dexpot\sudexpot -V", ProgramFiles, Hide
return
~RButton & XButton2::
Run, "dexpot\sudexpot-back.lnk", , Hide ;%ComSpec% "dexpot\sudexpot -V", ProgramFiles, Hide
return
~RButton & XButton1::
Run, "dexpot\sudexpot-f.lnk", , Hide ;%ComSpec% "dexpot\sudexpot -V", ProgramFiles, Hide
return


;RButton Up::Click Up Right 


DoubleTapDexpot(command, key) {
	static HotkeyRepeatCount = 0
	If (A_ThisHotKey = A_PriorHotkey && A_TimeSincePriorHotkey < 250 && (++HotkeyRepeatCount = 2))
	{
		Run, "dexpot\sudexpot%command%.lnk", , Hide ;%ComSpec% "dexpot\sudexpot -V", ProgramFiles, Hide
		HotkeyRepeatCount := 0
		return 1
	}
	Else
	{
		KeyWait %key%
		HotkeyRepeatCount := 1
		return 0
	}
}

LCtrl & Escape::Send ^+{Escape}
LCtrl::DoubleTapDexpot("-V", "LCtrl")
RCtrl & Escape::Send ^+{Escape}
RCtrl::DoubleTapDexpot("-d", "RCtrl")
LShift & Escape::Send +{Escape}
LShift::DoubleTapDexpot("-back", "LShift")
RShift & Escape::Send +{Escape}
RShift::DoubleTapDexpot("_v", "RShift")
;return



;Right ctrl find window
;left shift win cat all
;right shift win cat active