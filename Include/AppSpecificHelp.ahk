TitleMatchModes := ""
ST_Dim(TitleMatchModes)
PushTitleMatchMode(Mode) {
	global TitleMatchModes
	ST_Push(TitleMatchModes, A_TitleMatchMode)
	SetTitleMatchMode % Mode
	return
}
PopTitleMatchMode() {
	global TitleMatchModes
	Mode := ST_Pop(TitleMatchModes)
	MsgBox % Mode
	SetTitleMatchMode % Mode
	return Mode
}

$F1::
PushTitleMatchMode(2)
If (WinActive("AutoHotKey"))
{
	Run % A_ProgramFiles . "\AutoHotKey\AutoHotKey.chm"
}
else
{
	Send {F1}
}
PopTitleMatchMode()
return