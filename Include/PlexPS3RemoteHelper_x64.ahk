;Written by EliteGamer360, Please give credit When you Copy and use my scripts for any other purpose.

; #NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
; SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; #SingleInstance, Force

^!+P::	;Ctrl-Alt-Shift P is the shortcut for greenbutton_Win64bit
	IfWinNotExist Plex Home Theater.exe ;If Plex is not running
    run "C:\Program Files (x86)\Plex Home Theater\Plex Home Theater.exe"

	WinActivate Plex Home Theater ;Activate and Refocus Plex.
	WinShow Plex Home Theater ;Bring Plex to front.
	WinGet, Style, Style, ahk_class Plex
	if (Style & 0xC00000)  ;0xC00000 is WS_CAPTION, meaning window has a title bar.
	{
	    Send {VKDC}  ;Maximize Plex to fullscreen mode if its in a window mode.
	}
	Return


  SetTitleMatchMode 2
  #IfWinActive Plex ahk_class Plex ; Plex detection for Plex/GSB Home Screen action.
  ^!Enter::
  WinGet, Style, Style, ahk_class Plex
  if (Style & 0xC00000)  ;0xC00000 is WS_CAPTION, meaning window has a title bar.
  {
        Send {VKDC}  ;Maximize Plex to fullscreen mode if its in a window mode.
  }
  ;WinMaximize ;Maximize Plex if Windowed.
  send, ^!{VK74}  ; if Plex is Active (GSB Home Jump will activate)
  Return
  #IfWinActive ;
  
  
; Win+F12
;#F12::
;    ; Sleep/Suspend:
;    DllCall("PowrProf\SetSuspendState", "int", 0, "int", 0, "int", 0)
;    Return
