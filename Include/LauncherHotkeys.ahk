Morse(timeout = 250) { ; By Laszlo
   tout := timeout/1000
   key := RegExReplace(A_ThisHotKey,"[\*\~\$\#\+\!\^]")
   Loop {
      t := A_TickCount
      KeyWait %key%
      Pattern .= A_TickCount-t > timeout
      KeyWait %key%,DT%tout%
      If (ErrorLevel)
         Return Pattern
   }
}

F14::
pat := Morse() 
If (pat = "0")
   Send ^#{F12}
Else If (pat = "00")
   Send ^!{F3}
Else If (pat = "1")
   Send #g
Else MsgBox pat
Return

;~ LCtrl & Escape::Send ^+{Escape}
;~ LCtrl::DoubleTapDexpot("V", "LCtrl")
;~ RCtrl & Escape::Send ^+{Escape}
;~ RCtrl::DoubleTapDexpot("v", "RCtrl")
;~ LShift & Escape::Send +{Escape}
;~ LShift::DoubleTapDexpot("back", "LShift") 

;LaunchyPath = must be set in the autorun section
;LaunchyPath := "C:\Program Files (x86)\Launchy\Launchy.exe"



;Right ctrl find window
;left shift win cat all
;right shift win cat active