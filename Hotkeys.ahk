ListLines, Off  ;Off for daily use, On for debugging
#MenuMaskKey F23
#NoEnv
#Persistent
#SingleInstance
#KeyHistory 0 
;0 for daily use, more for debugging
#WinActivateForce

SendMode InputThenPlay

;HomeFolder := "Not Used at Present"
;EnvGet, HomeFolder, AHKHOME 
SetWorkingDir %A_ScriptDir% 

;Run ConfineMouse.ahk
return


#F5::
Reload
return

;#Include Include\ConfineMouse.ahk
;#Include Cut.ahk
#Include Include\DoubleEscapeToClose.ahk
#Include Include\PlexPS3RemoteHelper_x64.ahk
#Include Include\DexpotGestures.ahk

;#Include Lib\RapidHotkey.ahk
#Include Include\LauncherHotkeys.ahk

#Include Include\WinKeyOverrides.ahk
#Include Include\WinControl.ahk
#Include Include\MoveInactiveWin.ahk
;#Include Include\AppSpecificHelp.ahk
#Include Include\HotstringHelper.ahk

#Include ..\Personal\Hotstrings.ahk

;^!t::
;IfWinExist Untitled - Notepad
;	WinActivate
;else
;	Run Sublime_Text
;return

;#^c::
;Send ^c
;StringLen len, clipboard
;Tooltip %len%
;SetTimer, RemoveToolTip, 2000
;return

;RemoveToolTip:
;SetTimer, RemoveToolTip, Off
;ToolTip
;return

;#^v::
;Send ^c
;val := clipboard
;clipboard =
;clipboard := val -22
;clipwait
;Send ^v
;return

