ListLines, Off  ;Off for daily use, On for debugging
#MenuMaskKey vk07
#NoEnv
#KeyHistory 0   ;0 for daily use, more for debugging
#WinActivateForce

SetWorkingDir %A_ScriptDir% 

StringReplace, toRun, A_ScriptFullPath, .exe, x.exe
toRun := "ele.exe " . toRun
Loop, %0%  ; For each parameter:
{
    toRun := toRun . " " . %A_Index%  ; Fetch the contents of the variable whose name is contained in A_Index.
}
Run, %toRun%, , Hide


